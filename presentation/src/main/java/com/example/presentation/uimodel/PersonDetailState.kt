package com.example.presentation.uimodel

data class PersonDetailState(
    val isLoading: Boolean = false,
    val personDetails: PersonUiModel? = null,
    val error: String = ""
)
