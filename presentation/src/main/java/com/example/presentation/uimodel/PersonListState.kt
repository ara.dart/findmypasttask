package com.example.presentation.uimodel

data class PersonListState(
    val isLoading: Boolean = false,
    val personList: List<PersonUiModel>? = null,
    val error: String = ""
)
