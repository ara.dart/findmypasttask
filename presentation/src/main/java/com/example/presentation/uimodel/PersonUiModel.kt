package com.example.presentation.uimodel

import com.example.domain.entities.persondetailed.Relationships

data class PersonUiModel(
    var id: String,
    var firstname: String,
    var surname: String,
    var dob: String,
    var image: String? = null,
    var relationships: Relationships? = null,
    var relationType: String? = null

)

fun PersonUiModel.getRelations(): ArrayList<String> {
    val relatives = arrayListOf<String>()
    relationships?.children?.forEach {
        relatives.add(it)
    }
    relationships?.spouse?.let { relatives.add(it) }
    relationships?.mother?.let { relatives.add(it) }
    relationships?.father?.let { relatives.add(it) }
    return relatives
}


