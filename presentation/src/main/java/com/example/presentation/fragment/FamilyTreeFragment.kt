package com.example.presentation.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.presentation.adapter.FamilyTreeAdapter
import com.example.presentation.databinding.FragmentFamilyTreeBinding
import com.example.presentation.uimodel.getRelations
import com.example.presentation.viewmodel.FamilyTreeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

const val REQUEST_DELAY_L = 500L

class FamilyTreeFragment : Fragment() {

    private lateinit var binding: FragmentFamilyTreeBinding

    private val familyTreeViewModel by viewModel<FamilyTreeViewModel>()
    private val args: FamilyTreeFragmentArgs by navArgs()

    private lateinit var familyTreeAdapter: FamilyTreeAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFamilyTreeBinding.inflate(inflater, container, false)
        setUpRecyclerView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var relatives = arrayListOf<String>()

        familyTreeViewModel.apply {
            getPersonDetails(args.userIdArg, args.userSurnameArg)
            state.observe(viewLifecycleOwner) { personDetailsState ->

                adjustProgressBar(personDetailsState.isLoading)

                personDetailsState.personDetails?.let { personDetails ->
                    relatives = personDetails.getRelations()
                    familyTreeAdapter.addPerson(personDetails)
                }
            }

            binding.rvPersons.postDelayed({
                relatives.forEach {
                    familyTreeViewModel.getPersonDetails(
                        it,
                        args.userSurnameArg
                    )
                }
            }, REQUEST_DELAY_L)
        }
    }

    private fun setUpRecyclerView() {
        val rVCities = binding.rvPersons
        rVCities.layoutManager = LinearLayoutManager(requireContext())
        familyTreeAdapter = FamilyTreeAdapter()
        rVCities.adapter = familyTreeAdapter

    }

    private fun adjustProgressBar(isLoading: Boolean) {
        if (isLoading) {
            binding.parentContainer.visibility = View.INVISIBLE
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.INVISIBLE
            binding.parentContainer.visibility = View.VISIBLE
        }
    }

}