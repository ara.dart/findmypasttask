package com.example.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.presentation.databinding.FragmentUserIdEntryBinding


class UserIdEntryFragment : Fragment() {

    private lateinit var binding: FragmentUserIdEntryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserIdEntryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.userIdSubmit.setOnClickListener {
            if (binding.userIdInput.text.isEmpty()) {
                showEntryError()
            } else {
                findNavController().navigate(
                    UserIdEntryFragmentDirections.actionUserIdEntryFragmentToPersonListFragment(
                        binding.userIdInput.text.toString()
                    )
                )
            }

        }
    }

    private fun showEntryError() {
        binding.userIdInput.error = "The input is empty"
    }
}