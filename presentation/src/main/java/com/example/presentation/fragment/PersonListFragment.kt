package com.example.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.presentation.adapter.PersonListAdapter
import com.example.presentation.databinding.FragmentPersonListBinding
import com.example.presentation.viewmodel.PersonListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class PersonListFragment : Fragment() {

    private lateinit var binding: FragmentPersonListBinding
    private lateinit var adapter: PersonListAdapter

    private val personListViewModel by viewModel<PersonListViewModel>()
    private val arg: PersonListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPersonListBinding.inflate(inflater, container, false)
        setUpRecyclerView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        personListViewModel.apply {
            getPersonList(arg.userIdArg)
            state.observe(viewLifecycleOwner) { personListsTate ->
                adjustProgressBar(personListsTate.isLoading)
                personListsTate.personList?.let { personList -> adapter.setPersonList(personList) }
            }
        }
    }

    private fun setUpRecyclerView() {
        val rVCities = binding.rvPersons
        rVCities.layoutManager = LinearLayoutManager(requireContext())
        adapter = PersonListAdapter { selectedPersonId ->
            findNavController().navigate(
                PersonListFragmentDirections.actionPersonListFragmentToFamilyTreeFragment(
                    arg.userIdArg,
                    selectedPersonId
                )
            )
        }
        rVCities.adapter = adapter

    }

    private fun adjustProgressBar(isLoading: Boolean) {
        if (isLoading) {
            binding.parentContainer.visibility = View.INVISIBLE
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.INVISIBLE
            binding.parentContainer.visibility = View.VISIBLE
        }
    }

}