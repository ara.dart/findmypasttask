package com.example.presentation.adapter

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DateParser {
    @SuppressLint("SimpleDateFormat")
    fun getDateTimeAsFormattedString(dateAsLongInMs: Long): String? {
        return try {
            SimpleDateFormat("dd/mm/yyyy").format(Date(dateAsLongInMs))
        } catch (e: Exception) {
            null // parsing exception
        }
    }
}