package com.example.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.presentation.R
import com.example.presentation.adapter.DateParser.getDateTimeAsFormattedString
import com.example.presentation.databinding.ItemPersonListBinding
import com.example.presentation.uimodel.PersonUiModel
import java.text.SimpleDateFormat
import java.util.*

class PersonListAdapter(private val onCityItemClick: (String) -> Unit) :
    RecyclerView.Adapter<PersonListAdapter.PersonListViewHolder>() {

    private val personList = arrayListOf<PersonUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonListViewHolder {
        val binding =
            ItemPersonListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PersonListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PersonListViewHolder, position: Int) {


        val personItem = personList[position]


        holder.personName.text = personItem.firstname
        holder.personSurname.text = personItem.surname
        if (personItem.dob.isNotEmpty()) {
            holder.personDOB.text = getDateTimeAsFormattedString(personItem.dob.toLong())
        }

        holder.personImage.load(personItem.image) {
            crossfade(true)
            error(R.drawable.ic_avatar_placeholder)
            transformations(CircleCropTransformation())
        }

        holder.itemView.setOnClickListener {
            onCityItemClick.invoke(personItem.id)
        }

    }

    override fun getItemCount() = personList.size

    class PersonListViewHolder(binding: ItemPersonListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val personImage = binding.personAvatar
        val personName = binding.personFirstname
        val personSurname = binding.personSurname
        val personDOB = binding.personDOB
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPersonList(persons: List<PersonUiModel>) {
        personList.clear()
        personList.addAll(persons)
        notifyDataSetChanged()

    }
}

