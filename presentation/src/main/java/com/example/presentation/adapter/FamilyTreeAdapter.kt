package com.example.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.presentation.R
import com.example.presentation.databinding.ItemPersonFamilyTreeBinding
import com.example.presentation.uimodel.PersonUiModel

class FamilyTreeAdapter :
    RecyclerView.Adapter<FamilyTreeAdapter.FamilyTreeViewHolder>() {

    private val personDetailsList = arrayListOf<PersonUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FamilyTreeViewHolder {
        val binding =
            ItemPersonFamilyTreeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FamilyTreeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FamilyTreeViewHolder, position: Int) {

        val personItem = personDetailsList[position]

        holder.personName.text = personItem.firstname
        holder.personSurname.text = personItem.surname
        if (personItem.dob.isNotEmpty()) {
            holder.personDOB.text = DateParser.getDateTimeAsFormattedString(personItem.dob.toLong())
        }

        holder.personImage.load(personItem.image) {
            crossfade(true)
            error(R.drawable.ic_avatar_placeholder)
            transformations(CircleCropTransformation())
        }

    }

    override fun getItemCount() = personDetailsList.size

    class FamilyTreeViewHolder(binding: ItemPersonFamilyTreeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val personImage = binding.personAvatar
        val personName = binding.personFirstname
        val personSurname = binding.personSurname
        val personDOB = binding.personDOB
    }

    fun addPerson(person: PersonUiModel) {
        personDetailsList.add(person)
        // notifyDataSetChanged()
        notifyItemChanged(personDetailsList.size - 1)

    }
}

