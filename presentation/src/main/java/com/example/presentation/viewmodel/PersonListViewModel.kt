package com.example.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.common.Resource
import com.example.domain.usecase.GetPersonListByIdUseCase
import com.example.presentation.mapper.PersonToUiMapper
import com.example.presentation.uimodel.PersonListState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class PersonListViewModel(
    private val getPersonListByIdUseCase: GetPersonListByIdUseCase,
    private val personToUiMapper: PersonToUiMapper
) :
    ViewModel() {

    private val _state = MutableLiveData<PersonListState>()
    val state: MutableLiveData<PersonListState> = _state

    fun getPersonList(userId: String) {
        getPersonListByIdUseCase.execute(userId).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = PersonListState(personList = result.data?.let {
                        personToUiMapper.mapFrom(
                            it
                        )
                    })
                }
                is Resource.Error -> {
                    _state.value =
                        PersonListState(error = result.message ?: "An unexpected error occured")
                }
                is Resource.Loading -> {
                    _state.value = PersonListState(isLoading = true)
                }
            }

        }.launchIn(viewModelScope)
    }
}