package com.example.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.common.Resource
import com.example.domain.usecase.GetPersonDetailsByIdUseCase
import com.example.presentation.mapper.PersonDetailsToUiMapper
import com.example.presentation.uimodel.PersonDetailState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FamilyTreeViewModel(
    private val getPersonDetailsByIdUseCase: GetPersonDetailsByIdUseCase,
    private val personDetailsToUiMapper: PersonDetailsToUiMapper
) :
    ViewModel() {

    private val _state = MutableLiveData<PersonDetailState>()
    val state: MutableLiveData<PersonDetailState> = _state

    fun getPersonDetails(userId: String, userFamilyId: String) {
        getPersonDetailsByIdUseCase.execute(userId, userFamilyId).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = PersonDetailState(personDetails = result.data?.let {
                        personDetailsToUiMapper.mapFrom(
                            it
                        )
                    })
                }
                is Resource.Error -> {
                    _state.value =
                        PersonDetailState(error = result.message ?: "An unexpected error occured")
                }
                is Resource.Loading -> {
                    _state.value = PersonDetailState(isLoading = true)
                }
            }

        }.launchIn(viewModelScope)
    }
}