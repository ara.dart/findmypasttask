package com.example.presentation.mapper

import com.example.domain.entities.person.Person
import com.example.presentation.uimodel.PersonUiModel

class PersonToUiMapper {
    fun mapFrom(personList: List<Person>): List<PersonUiModel> {
        return personList.map {
            PersonUiModel(
                id = it.id,
                firstname = it.firstname,
                surname = it.surname,
                dob = it.dob,
                image = it.image
            )
        }
    }
}