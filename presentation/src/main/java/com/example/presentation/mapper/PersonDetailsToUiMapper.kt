package com.example.presentation.mapper

import com.example.domain.entities.persondetailed.PersonDetails
import com.example.presentation.uimodel.PersonUiModel

class PersonDetailsToUiMapper {
    fun mapFrom(person: PersonDetails): PersonUiModel {
        return PersonUiModel(
            id = person.id,
            firstname = person.firstname,
            surname = person.surname,
            dob = person.dob,
            image = person.image,
            relationships = person.relationships
        )
    }
}