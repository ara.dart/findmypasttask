package com.example.data.repository.remote

import com.example.data.api.PersonApi
import com.example.data.api.dto.person.mapToPersonList
import com.example.data.api.dto.persondetails.mapToPersonDetails
import com.example.domain.entities.person.Person
import com.example.domain.entities.persondetailed.PersonDetails
import com.example.domain.repository.PersonRemoteDataSource

class PersonRemoteDataSourceImpl(private val personApi: PersonApi) : PersonRemoteDataSource {
    override suspend fun getPersonList(userId: String): List<Person> {
        return personApi.getPersonList(userId).mapToPersonList()
    }

    override suspend fun getPersonDetails(userId: String, userFamilyId: String): PersonDetails {
        return personApi.getPersonDetails(userId, userFamilyId).mapToPersonDetails()
    }
}