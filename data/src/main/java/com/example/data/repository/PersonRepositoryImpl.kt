package com.example.data.repository

import com.example.domain.entities.person.Person
import com.example.domain.entities.persondetailed.PersonDetails
import com.example.domain.repository.PersonRemoteDataSource
import com.example.domain.repository.PersonRepository

class PersonRepositoryImpl(private val personRemoteDataSource: PersonRemoteDataSource) :
    PersonRepository {
    override suspend fun getPersonList(userId: String): List<Person> {
        return personRemoteDataSource.getPersonList(userId)
    }

    override suspend fun getPersonDetails(userId: String, userFamilyId:String): PersonDetails {
        return personRemoteDataSource.getPersonDetails(userId, userFamilyId)
    }

}