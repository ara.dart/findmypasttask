package com.example.data.api.dto.person

import com.example.domain.entities.person.Person
import com.google.gson.annotations.SerializedName

data class PersonListDto(
    @SerializedName("success") val success: Boolean? = null,
    @SerializedName("data") val personsDto: List<PersonDto>
)

fun PersonListDto.mapToPersonList(): List<Person> {
    return personsDto.map {
        it.mapToPerson()
    }
}