package com.example.data.api.dto.persondetails

import com.example.domain.entities.persondetailed.Relationships
import com.google.gson.annotations.SerializedName


data class RelationshipsDto(

    @SerializedName("spouse") val spouse: String,
    @SerializedName("mother") val mother: String,
    @SerializedName("father") val father: String,
    @SerializedName("children") val children: List<String>

)

fun RelationshipsDto.mapToRelationships(): Relationships {
    return Relationships(spouse = spouse, mother = mother, father = father, children = children)
}

