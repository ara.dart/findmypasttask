package com.example.data.api

import com.example.data.api.dto.person.PersonListDto
import com.example.data.api.dto.persondetails.PersonDetailsContainerDto
import retrofit2.http.GET
import retrofit2.http.Path

interface PersonApi {
    @GET("profiles/{userId}/")
    suspend fun getPersonList(@Path("userId") userId: String): PersonListDto

    @GET("profile/{userId}/{userFamilyId}")
    suspend fun getPersonDetails(
        @Path("userId") userId: String,
        @Path("userFamilyId") userFamilyId: String
    ): PersonDetailsContainerDto

}

