package com.example.data.api.dto.persondetails

import com.example.domain.entities.persondetailed.PersonDetails
import com.google.gson.annotations.SerializedName

data class PersonDetailsDto(

    @SerializedName("id") val id: String,
    @SerializedName("firstname") val firstname: String,
    @SerializedName("surname") val surname: String,
    @SerializedName("dob") val dob: String,
    @SerializedName("image") val image: String,
    @SerializedName("relationships") val relationshipsDto: RelationshipsDto

)

fun PersonDetailsDto.mapToPersonDetails(): PersonDetails {
    return PersonDetails(
        id = id,
        firstname = firstname,
        surname = surname,
        dob = dob,
        image = image,
        relationships = relationshipsDto.mapToRelationships()
    )
}