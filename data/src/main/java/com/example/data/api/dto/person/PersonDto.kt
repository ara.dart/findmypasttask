package com.example.data.api.dto.person


import com.example.domain.entities.person.Person
import com.google.gson.annotations.SerializedName

data class PersonDto(
    @SerializedName("id") val id: String,
    @SerializedName("firstname") val firstname: String,
    @SerializedName("surname") val surname: String,
    @SerializedName("dob") val dob: String,
    @SerializedName("image") val image: String? = null
)

fun PersonDto.mapToPerson(): Person {
    return Person(
        id = id,
        firstname = firstname,
        surname = surname,
        dob = dob,
        image = image
    )
}