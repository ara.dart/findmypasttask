package com.example.data.api.dto.persondetails

import com.example.domain.entities.persondetailed.PersonDetails
import com.google.gson.annotations.SerializedName

data class PersonDetailsContainerDto(

    @SerializedName("success") val success: Boolean,
    @SerializedName("data") val personDetailsDto: PersonDetailsDto

)

fun PersonDetailsContainerDto.mapToPersonDetails(): PersonDetails {
    return personDetailsDto.mapToPersonDetails()
}