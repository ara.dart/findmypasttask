package com.example.findmypasttask.di


import com.example.presentation.mapper.PersonDetailsToUiMapper
import com.example.presentation.mapper.PersonToUiMapper
import com.example.presentation.viewmodel.FamilyTreeViewModel
import com.example.presentation.viewmodel.PersonListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    single { PersonToUiMapper() }
    single { PersonDetailsToUiMapper() }
    viewModel {
        PersonListViewModel(
            getPersonListByIdUseCase = get(),
            personToUiMapper = get()
        )
    }
    viewModel {
        FamilyTreeViewModel(
            getPersonDetailsByIdUseCase = get(),
            personDetailsToUiMapper = get()
        )
    }

}