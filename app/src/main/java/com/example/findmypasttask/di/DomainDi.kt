package com.example.findmypasttask.di

import com.example.domain.usecase.GetPersonDetailsByIdUseCase
import com.example.domain.usecase.GetPersonListByIdUseCase
import org.koin.dsl.module

val domainModule = module {
    factory { GetPersonDetailsByIdUseCase(personRepository = get()) }
    factory { GetPersonListByIdUseCase(personRepository = get()) }
}