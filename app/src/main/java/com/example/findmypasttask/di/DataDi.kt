package com.example.findmypasttask.di

import com.example.data.api.PersonApi
import com.example.data.repository.PersonRepositoryImpl
import com.example.data.repository.remote.PersonRemoteDataSourceImpl
import com.example.domain.repository.PersonRemoteDataSource
import com.example.domain.repository.PersonRepository
import com.example.findmypasttask.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {
    single { provideOkHttpClient() }
    single { provideWeatherApi(retrofit = get()) }
    single { provideRetrofit(okHttpClient = get()) }
    factory<PersonRepository> { PersonRepositoryImpl(personRemoteDataSource = get()) }
    factory<PersonRemoteDataSource> { PersonRemoteDataSourceImpl(personApi = get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()
}

fun provideWeatherApi(retrofit: Retrofit): PersonApi = retrofit.create(PersonApi::class.java)