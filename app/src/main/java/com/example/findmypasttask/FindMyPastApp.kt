package com.example.findmypasttask

import android.app.Application
import android.provider.Settings
import com.example.findmypasttask.di.dataModule
import com.example.findmypasttask.di.domainModule
import com.example.findmypasttask.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class FindMyPastApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@FindMyPastApp)
            modules(listOf(domainModule, dataModule, presentationModule))
        }
    }
}