package com.example.domain.entities.person

data class Person(
    var id: String,
    var firstname: String,
    var surname: String,
    var dob: String,
    var image: String? = null
)