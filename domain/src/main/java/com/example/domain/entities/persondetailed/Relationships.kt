package com.example.domain.entities.persondetailed

data class Relationships(
    val spouse: String?,
    var mother: String?,
    var father: String?,
    var children: List<String>?,
)