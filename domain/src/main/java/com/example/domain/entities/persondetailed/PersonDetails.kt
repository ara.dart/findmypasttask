package com.example.domain.entities.persondetailed

data class PersonDetails(
    var id: String,
    var firstname: String,
    var surname: String,
    var dob: String,
    var image: String? = null,
    var relationships: Relationships? = null
)