package com.example.domain.usecase

import com.example.domain.common.Resource
import com.example.domain.entities.person.Person
import com.example.domain.repository.PersonRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException

class GetPersonListByIdUseCase(private val personRepository: PersonRepository) {

    fun execute(userId: String): Flow<Resource<List<Person>>> =
        flow {
            try {
                emit(Resource.Loading())
                val personList = personRepository.getPersonList(userId)
                emit(Resource.Success(personList))
            } catch (e: IOException) {
                emit(Resource.Error("Couldn't reach server, Check your internet connection"))
            } catch (e: Exception) {
                emit(Resource.Error(e.localizedMessage ?: "An Unexpected error ocuured"))
            }
        }
}
