package com.example.domain.usecase

import com.example.domain.common.Resource
import com.example.domain.entities.persondetailed.PersonDetails
import com.example.domain.repository.PersonRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException

class GetPersonDetailsByIdUseCase(private val personRepository: PersonRepository) {

    fun execute(userId: String, userFamilyId: String): Flow<Resource<PersonDetails>> =
        flow {
            try {
                emit(Resource.Loading())
                val personLDetails = personRepository.getPersonDetails(userId, userFamilyId)
                emit(Resource.Success(personLDetails))
            } catch (e: IOException) {
                emit(Resource.Error("Couldn't reach server, Check your internet connection"))
            } catch (e: Exception) {
                emit(Resource.Error(e.localizedMessage ?: "An Unexpected error ocuured"))
            }
        }
}