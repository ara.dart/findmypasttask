package com.example.domain.repository

import com.example.domain.entities.person.Person
import com.example.domain.entities.persondetailed.PersonDetails

interface PersonRemoteDataSource {
    suspend fun getPersonList(userId: String): List<Person>
    suspend fun getPersonDetails(userId: String, userFamilyId:String): PersonDetails
}